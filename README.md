Week 7 Project: WordPress vs. Kali
1. (Required) User Enumeration
  - [X] Summary: I used user enumeration and brute force to access the admin username and password. 
		 Since a default password was used, this was pretty straight forward. 
    - Vulnerability types: User Enumeration
    - Tested in version: 4.2
    - Fixed in version: N/A
  - [X] GIF Walkthrough: 
    <img src='https://i.imgur.com/PINQ6FB.gif' width="800">
                         
    <img src='https://i.imgur.com/HdCQVTF.gif' width="800">

  - [X] Steps to recreate: 
	1. Enumerate users with the command "wpscan --url http://wpdistillery.vm/ --enumerate u"
	2. Determine what username to brute force. Here I noticed an "admin" username, so I thought the
	   username would be a default. 
	3. Brute force the password with the command "wpscan --url http://wpdistillery.vm --wordlist 
	   /usr/share/wordlists/small.txt --username admin"
  - [X] Affected source code: N/A

2. (Required) XSS
  - [X] Summary: XSS can be accomplished by naming a title of a page with image source script. This 
		 is because the input for the title is not sanitized properly.  
    - Vulnerability types: XSS
    - Tested in version: 4.2
    - Fixed in version: N/A
  - [X] GIF Walkthrough: <img src='https://i.imgur.com/kFN60ee.gif' width="800">
                         <img src='https://i.imgur.com/P4ZGKpB.gif' width="800">
  - [X] Steps to recreate: 
	1. Name a page with "<IMG SRC="#" ONERROR="alert('XSS')" added.
	2. Change the page to an image page
	3. Visit the homepage
  - [X] Affected source code: N/A
    
3. (Required) XSS
  - [ ] Summary: The input for the post in the content section is not santizied properly. 
    - Vulnerability types: XSS
    - Tested in version: 4.2.2
    - Fixed in version: 4.2.3
  - [ ] GIF Walkthrough:   <img src='https://i.imgur.com/V4VmH74.gif' width="800">
  - [ ] Steps to recreate: 
	1. Add the following command to a post in plain text as an href in <a> tags: title=" 
	   onmouseover=alert('abc')">link 
	2. Have an admin visit the post.
  - [ ] Affected source code: 